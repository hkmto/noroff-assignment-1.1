﻿namespace MyAssignemnt
{
    public class SecondaryAttributes
    {
        public int Health { get; set; }
        public int ArmourRating { get; set; }
        public int ElementalResistance { get; set; }

    }
}
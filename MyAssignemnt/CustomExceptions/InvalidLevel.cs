﻿using System;
using System.Runtime.Serialization;

namespace MyAssignemnt
{
    [Serializable]
    internal class InvalidLevel : Exception
    {
        public InvalidLevel()
        {
        }

        public InvalidLevel(string message) : base(message)
        {
        }

        public InvalidLevel(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidLevel(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
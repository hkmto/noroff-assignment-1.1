﻿using System;
using System.Runtime.Serialization;

namespace MyAssignemnt
{
    [Serializable]
    internal class InvalidLevelForThisItem : Exception
    {
        public InvalidLevelForThisItem()
        {
        }

        public InvalidLevelForThisItem(string message) : base(message)
        {
        }

        public InvalidLevelForThisItem(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidLevelForThisItem(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
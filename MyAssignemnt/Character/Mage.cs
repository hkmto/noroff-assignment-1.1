﻿using MyAssignemnt.Items;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyAssignemnt
{
    public class Mage :Hero    
    {
        public Mage():base()
        {
            Attributes = new PrimaryAttributes { Strength = 1, Dexterity = 1, Intelligence = 8, Vitality = 5 };
        }
        public override void EquipWeapon(Weapon weapon)
        {
                         
          if (weapon.ItemLevel> Level) 
            
             throw new InvalidLevelForThisItem("Your hero's level is to low to equip this weapon");
            
            if (!(weapon.WeaponType == WeaponType.Staff || weapon.WeaponType == WeaponType.Wand))
            
             throw new InvalidWeaponType("This weapon is not allowed for this character");
            
            if (weapon.ItemSlot == Slot.Weapon)
            
                Inventory.Add(weapon.ItemSlot, weapon);
            
            else
               
                throw new InvalidSlotForWeapon("Your hero cannot equip this weapon in this slot");
                       
        }

        public override void EquipArmour(Armour armour) 
           
        {
            if (armour.ItemLevel > Level)
            
                throw new InvalidLevelForThisItem("Your hero's level is to low to equip this armour");
            
            if (armour.ArmourType != ArmourType.Cloth)
                
                throw new InvalidArmourType("This armour is not allowed for this character");


            if (armour.ItemSlot != Slot.Weapon)
                
                    Inventory.Add(armour.ItemSlot, armour);
                      
            else
            
                throw new InvalidSlotForArmour("Your hero cannot equip this armour in this slot");
        
        }
        
        public override void LevelUp(int level)
            
            {
            if (level < 1)
                throw new InvalidLevel("Level must be greater than 1");

                Level += level;
                Attributes.Strength = Attributes.Strength + 1;
                Attributes.Dexterity = Attributes.Dexterity + 1;
                Attributes.Intelligence = Attributes.Intelligence + 5;
                Attributes.Vitality = Attributes.Vitality + 3;
                
            }


         public override double CalculateHeroDamagePerSecond()
            {
           
                return CalculateEquippedWeaponDPS() * (1+ (double)TotalAttributes().Intelligence/ 100);
            }

        
    }
}

using System;
using Xunit;
using MyAssignemnt;
using MyAssignemnt.Items;


namespace MyAssignmentTestItem
{
    public class ItemTest
    {
        [Fact]
        public void EquipWeapon_EquippingTooHighLevelWeapon_ShouldThrowException()
        {
            //Arrange and act
            Warrior warrior = new Warrior();
            Weapon weapon = new Weapon()
            { ItemLevel = 4 };
            //Assert
            Assert.Throws<Exception>(() => warrior.EquipWeapon(weapon));


        }

        [Fact]
        public void EquipArmour_EquippingTooHighLevelArmour_ShouldThrowException()
        {
            //Arrange and act
            Warrior warrior = new Warrior();
            Armour armour = new Armour()
            { ItemLevel = 4 };
            //Assert
            Assert.Throws<Exception>(() => warrior.EquipArmour(armour));


        }

        [Fact]
        public void EquipArmour_EquippingWrongWeaponType_ShouldThrowException()
        {
            //Arrange and act
            Warrior warrior = new Warrior();
            Weapon weapon = new Weapon()
            { WeaponType = WeaponType.Bow };
            //Assert
            Assert.Throws<Exception>(() => warrior.EquipWeapon(weapon));


        }

        [Fact]
        public void EquipArmour_EquippingWrongArmourType_ShouldThrowException()
        {
            //Arrange and act
            Warrior warrior = new Warrior();
            Armour armour = new Armour()
            { ArmourType = ArmourType.Cloth };
            //Assert
            Assert.Throws<Exception>(() => warrior.EquipArmour(armour));
        }

        [Fact]
        public void CalculateDPS_CalculatingDPSWithNoWeapon_ShouldReturnBaseDPS()
        {
            //Arrange
            Warrior warrior = new Warrior();
            double expected = 1 * (1 + 5 / (double)100);

            //act

            double actual = warrior.CalculateHeroDamagePerSecond();

            //assert
            Assert.Equal(expected, actual);

        }
        [Fact]
        public void CalculateDPS_CalculatingDPSWithWeapon_ShouldReturnDPSWithWeaponEquipped()
        {
            //Arrange
            Warrior warrior = new Warrior();
            Weapon weapon = new Weapon()
            {ItemName="axe",ItemLevel=1,ItemSlot=Slot.Weapon, AttackSpeed =1.1,Damage=7 };
            double expected = (7 * 1.1) * (1 * (1 + 5 /(double)100));
            //act

            warrior.EquipWeapon(weapon);
            double actual= warrior.CalculateHeroDamagePerSecond();

            //assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void CalculateDPS_CalculatingDPSWithWeaponAndArmour_ShouldReturnDPSWithWeaponAndArmourEquipped()
        {
            //Arrange
            Warrior warrior = new Warrior();
            Weapon weapon = new Weapon() { ItemName = "axe", ItemLevel = 1, ItemSlot = Slot.Weapon, AttackSpeed = 1.1, Damage = 7 };
            var attrib = new PrimaryAttributes { Vitality = 2, Strength = 1 };
            Armour armour = new Armour() { ItemName="plate",ItemLevel=1,ItemSlot=Slot.Body,ArmourType=ArmourType.Plate,ArmourAttributes=attrib};
            double expected = (7 * 1.1) * (1 * (1 + 6 / (double)100));
            //act
            warrior.EquipWeapon(weapon);
            warrior.EquipArmour(armour);
            double actual = warrior.CalculateHeroDamagePerSecond();

            //assert
            Assert.Equal(expected, actual);

        }



    }
}

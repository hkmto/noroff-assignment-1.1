﻿using System;
using System.Runtime.Serialization;

namespace MyAssignemnt
{
    [Serializable]
    internal class InvalidArmourType : Exception
    {
        public InvalidArmourType()
        {
        }

        public InvalidArmourType(string message) : base(message)
        {
        }

        public InvalidArmourType(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidArmourType(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
﻿using MyAssignemnt.Items;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyAssignemnt
{
    public class Ranger : Hero
    {
        public Ranger()  
        {
            Attributes = new PrimaryAttributes { Strength = 1, Dexterity = 7, Intelligence = 1, Vitality = 8 };

        }

        public override void EquipWeapon(Weapon weapon)
            {

                if (weapon.ItemLevel > Level)

                    throw new InvalidLevelForThisItem("Your hero's level is to low to equip this weapon");

            if (weapon.WeaponType != WeaponType.Bow)

                    throw new InvalidWeaponType("This weapon is not allowed for this character");

            if (weapon.ItemSlot == Slot.Weapon)

                    Inventory.Add(weapon.ItemSlot, weapon);

                else

                throw new InvalidSlotForWeapon("Your hero cannot equip this weapon in this slot");

        }

            public override void EquipArmour(Armour armour)

            {
                if (armour.ItemLevel > Level)

                    throw new InvalidLevelForThisItem("Your hero's level is to low to equip this armour");

            if (!(armour.ArmourType == ArmourType.Mail || armour.ArmourType == ArmourType.Leather))

                throw new InvalidArmourType("This armour is not allowed for this character");


            if (armour.ItemSlot != Slot.Weapon)

                    Inventory.Add(armour.ItemSlot, armour);

                else

                    throw new InvalidSlotForArmour("Your hero cannot equip this armour in this slot");

        }

            public override void LevelUp(int level)

            {
                if (level < 1)
                    throw new InvalidLevel("Level must be greater than 1");

                Level += level;
                Attributes.Strength = Attributes.Strength + 1;
                Attributes.Dexterity = Attributes.Dexterity + 5;
                Attributes.Intelligence = Attributes.Intelligence + 1;
                Attributes.Vitality = Attributes.Vitality + 2;

            }


        public override double CalculateHeroDamagePerSecond()
        {

            return CalculateEquippedWeaponDPS() * (1 + (double)TotalAttributes().Dexterity / 100);
        }

    }
}

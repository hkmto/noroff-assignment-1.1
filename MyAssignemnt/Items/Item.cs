﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyAssignemnt.Items
{
    public abstract class Item
    {
        public string ItemName { get; set; }
        public int ItemLevel { get; set; }

        public Slot ItemSlot { get; set; }

    }

}

﻿using MyAssignemnt.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyAssignemnt
{
    public abstract class Hero
    {
        public string Name { get; set; }
        public int Level { get; set; } = 1;
        public PrimaryAttributes Attributes { get; set; }
        public SecondaryAttributes SecondaryAttributes { get; set; } = new SecondaryAttributes { };
        public double HeroDamagePrSecond { get; set; }
        public Dictionary<Slot, Item> Inventory { get; set; } = new Dictionary<Slot, Item>();



        public abstract void EquipWeapon(Weapon weapon);

        public abstract void EquipArmour(Armour armour);

        public abstract void LevelUp(int level);

        public abstract double CalculateHeroDamagePerSecond();

       
        public PrimaryAttributes CalculateEquippedArmour()
         {

            var armourPieces = Inventory.Where(x => x.Value.ItemSlot != Slot.Weapon)
                .Select(x => x.Value).Cast<Armour>();

            PrimaryAttributes totalAttributes = new PrimaryAttributes { };

            foreach (var item in armourPieces)
                {
                    totalAttributes.Strength += item.ArmourAttributes.Strength;
                    totalAttributes.Dexterity += item.ArmourAttributes.Dexterity;
                    totalAttributes.Intelligence += item.ArmourAttributes.Intelligence;
                    totalAttributes.Vitality += item.ArmourAttributes.Vitality;
                }
            return totalAttributes;

         }

        public double CalculateEquippedWeaponDPS()
        {

            var weaponPieces = Inventory.Where(x => x.Value.ItemSlot == Slot.Weapon)
                .Select(x => x.Value).Cast<Weapon>();

            double HeroDamagePrSecond=1;

            foreach (var item in weaponPieces)
            {
                HeroDamagePrSecond = item.Damage * item.AttackSpeed;

            }
            
            return HeroDamagePrSecond;

        }

        public void CalculateSecondaryAttributes()
        {
            var total = TotalAttributes();

            {
                SecondaryAttributes.Health = +(total.Vitality * 10);
                SecondaryAttributes.ArmourRating = +total.Strength + total.Dexterity;
                SecondaryAttributes.ElementalResistance = +total.Intelligence;
            };


        }



        public PrimaryAttributes TotalAttributes()
        {
            var attributes = CalculateEquippedArmour();
            var total = new PrimaryAttributes
            {
                Dexterity = Attributes.Dexterity + attributes.Dexterity,
                Strength = Attributes.Strength + attributes.Strength,
                Intelligence = Attributes.Intelligence + attributes.Intelligence,
                Vitality = Attributes.Vitality + attributes.Vitality
            };
            return total;
        }


        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Your Hero "+Name +" is currently at level " + Level + " and have the following stats:" );
            sb.AppendLine("Strength:     "+TotalAttributes().Strength);
            sb.AppendLine("Dexterity:    " +TotalAttributes().Dexterity);
            sb.AppendLine("Intelligence: "+TotalAttributes().Intelligence);
            sb.AppendLine("Vitality:     "+TotalAttributes().Vitality);
            sb.AppendLine("Health:       " + SecondaryAttributes.Health);
            sb.AppendLine("ArmourRating: " + SecondaryAttributes.ArmourRating);
            sb.AppendLine("Elemetal resistance:" + SecondaryAttributes.ElementalResistance);
            sb.AppendLine("DPS:" + CalculateHeroDamagePerSecond());


            return sb.ToString();
        }




    }
}

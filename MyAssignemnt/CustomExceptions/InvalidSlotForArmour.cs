﻿using System;
using System.Runtime.Serialization;

namespace MyAssignemnt
{
    [Serializable]
    internal class InvalidSlotForArmour : Exception
    {
        public InvalidSlotForArmour()
        {
        }

        public InvalidSlotForArmour(string message) : base(message)
        {
        }

        public InvalidSlotForArmour(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidSlotForArmour(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
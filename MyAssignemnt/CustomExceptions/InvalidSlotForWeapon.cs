﻿using System;
using System.Runtime.Serialization;

namespace MyAssignemnt
{
    [Serializable]
    internal class InvalidSlotForWeapon : Exception
    {
        public InvalidSlotForWeapon()
        {
        }

        public InvalidSlotForWeapon(string message) : base(message)
        {
        }

        public InvalidSlotForWeapon(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidSlotForWeapon(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
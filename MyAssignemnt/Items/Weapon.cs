﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyAssignemnt.Items
{
    public class Weapon :Item  
    {
        public WeaponType WeaponType { get; set; }
        public double Damage { get; set; }
        public double AttackSpeed { get; set; }
    
        
    }
}

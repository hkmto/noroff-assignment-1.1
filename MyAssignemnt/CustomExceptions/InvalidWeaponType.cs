﻿using System;
using System.Runtime.Serialization;

namespace MyAssignemnt
{
    [Serializable]
    internal class InvalidWeaponType : Exception
    {
        public InvalidWeaponType()
        {
        }

        public InvalidWeaponType(string message) : base(message)
        {
        
        }

        public InvalidWeaponType(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidWeaponType(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
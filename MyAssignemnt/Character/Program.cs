﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using CsvHelper;
using MyAssignemnt.Items;

namespace MyAssignemnt
{
    class Program
    {
        static void Main(string[] args)
        {


            Mage mage = new Mage { Name = "Sean Spinner" };
            Rouge rouge = new Rouge { Name = "Courtny Above" };
            Warrior warrior = new Warrior { Name = "Donald Biden" };
            Ranger ranger = new Ranger { Name = "Dennis the Menace" };
            
            PrimaryAttributes att1 = new PrimaryAttributes { Dexterity = 2, Strength = 3, Intelligence = 7, Vitality = 3 };
            PrimaryAttributes att2 = new PrimaryAttributes { Dexterity = 4, Strength = 5, Intelligence = 4, Vitality = 5 };
            
            Weapon weapon =  new Weapon() {ItemName = "Staff of Agony",ItemLevel = 1,ItemSlot = Slot.Weapon, WeaponType = WeaponType.Wand, Damage = 6, AttackSpeed = 0.6};
            Weapon weapon1 = new Weapon() { ItemName = "Bow of accuracy", ItemLevel = 1, ItemSlot = Slot.Weapon, WeaponType = WeaponType.Bow, Damage = 9, AttackSpeed = 0.9 };
            Weapon weapon2 = new Weapon() { ItemName = "Dagger of precision", ItemLevel = 1, ItemSlot = Slot.Weapon, WeaponType = WeaponType.Dagger, Damage = 4, AttackSpeed = 1.5};
            Weapon weapon3 = new Weapon() { ItemName = "Axe of sorrow", ItemLevel = 1, ItemSlot = Slot.Weapon, WeaponType = WeaponType.Axe, Damage = 7, AttackSpeed = 1.1 };
            
            Armour armour =  new Armour() { ItemName = "Cloth of compassion", ItemLevel = 2, ItemSlot = Slot.Body, ArmourType = ArmourType.Cloth, ArmourAttributes = att1 };
            Armour armour1 = new Armour() { ItemName = "Leather of feather", ItemLevel = 2, ItemSlot = Slot.Head, ArmourType = ArmourType.Leather, ArmourAttributes = att1 };
            Armour armour2 = new Armour() { ItemName = "Plate of excellence", ItemLevel = 2, ItemSlot = Slot.Head, ArmourType = ArmourType.Plate, ArmourAttributes = att1 };
            Armour armour3 = new Armour() { ItemName = "You've got mail", ItemLevel = 2, ItemSlot = Slot.Legs, ArmourType = ArmourType.Mail, ArmourAttributes = att1 };
            Armour armour4 = new Armour() { ItemName = "Cloth of poor quality", ItemLevel = 2, ItemSlot = Slot.Head, ArmourType = ArmourType.Cloth, ArmourAttributes = att1 };


            mage.LevelUp(1);
            mage.LevelUp(1);
            mage.EquipWeapon(weapon);
            mage.EquipArmour(armour);
            mage.EquipArmour(armour4);
            mage.CalculateHeroDamagePerSecond();
            mage.TotalAttributes();
            mage.CalculateSecondaryAttributes();
            Console.WriteLine(mage);


            rouge.LevelUp(1);
            rouge.LevelUp(1);
            rouge.EquipWeapon(weapon2);
            rouge.EquipArmour(armour1);
            rouge.EquipArmour(armour3);
            rouge.CalculateHeroDamagePerSecond();
            rouge.TotalAttributes();
            rouge.CalculateSecondaryAttributes();
            Console.WriteLine(rouge);

            ranger.LevelUp(1);
            ranger.LevelUp(1);
            ranger.EquipWeapon(weapon1);
            ranger.EquipArmour(armour1);
            ranger.EquipArmour(armour3);
            ranger.CalculateHeroDamagePerSecond();
            ranger.TotalAttributes();
            ranger.CalculateSecondaryAttributes();
            Console.WriteLine(ranger);

            warrior.LevelUp(1);
            warrior.LevelUp(1);
            warrior.EquipWeapon(weapon3);
            warrior.EquipArmour(armour2);
            warrior.EquipArmour(armour3);
            warrior.CalculateHeroDamagePerSecond();
            warrior.TotalAttributes();
            warrior.CalculateSecondaryAttributes();
            Console.WriteLine(warrior);

            

        }

    }
}

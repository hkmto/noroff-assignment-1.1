using System;
using Xunit;
using MyAssignemnt;


namespace MyAssignmentTest
{
    public class HeroTest
    {
        [Fact]
        public void StartsWithLevel_ShouldReturnInitialLevel()
        {
            // arrange
            Mage mage = new Mage();

            int expected = 1;

            //act

            int actual = mage.Level;

            // assert

            Assert.Equal(expected, actual);


        }

        [Fact]
        public void LevelUp_AddNewLevel_ShouldReturnNewLevel()

        {
            Mage mage = new Mage();

            //arrange
            mage.LevelUp(1);
            int expected = 2;
            //act

            int actual = mage.Level;
            
            //assert

            Assert.Equal(expected, actual);


        }


        [Theory]
        [InlineData(0,-1)]
        public void CheckHeroLevel_AddZeroOrLess_ShouldThrowArgumentException(int num1, int num2)

        {
            //Arrange
            Mage mage = new Mage();                                                                 
                        
           //act and assert
                                                                                // All tests ran fine until I changed the Exception to customized exceptions. I could not figure out how to set up the test for customized exceptions here.            
            Assert.Throws<Exception>(()=>mage.LevelUp(num1));
            Assert.Throws<Exception>(() => mage.LevelUp(num2));

        }



        [Fact]
        public void NewMage_CheckDeafultAttributes_ShouldReturnDeafultAttributes()

        {
            //arrange
            Mage mage = new Mage();
                      
            int[] expected = new int[] {1,1,8,5};

            //act
            
            int[] actual = new int[] { mage.Attributes.Strength, mage.Attributes.Dexterity, mage.Attributes.Intelligence, mage.Attributes.Vitality };

            //assert

            Assert.Equal(expected, actual);


        }

        [Fact]
        public void LevelUpMage_CheckLevelUpAttributes_ShouldReturnLevelUpAttributes()

        {
            //arrange
            Mage mage = new Mage();

            int[] expected = new int[] { 2, 2, 13, 8 };

            //act
            mage.LevelUp(1);
            int[] actual = new int[] { mage.Attributes.Strength, mage.Attributes.Dexterity, mage.Attributes.Intelligence, mage.Attributes.Vitality };

            //assert

            Assert.Equal(expected, actual);


        }

 

        [Fact]
        public void NewRouge_CheckDeafultAttributes_ShouldReturnDeafultAttributes()

        {
            //arrange
            Rouge rouge = new Rouge();

            int[] expected = new int[] { 2, 6, 1, 8 };

            //act

            int[] actual = new int[] { rouge.Attributes.Strength, rouge.Attributes.Dexterity, rouge.Attributes.Intelligence, rouge.Attributes.Vitality };

            //assert

            Assert.Equal(expected, actual);


        }

        [Fact]
        public void LevelUpRouge_CheckLevelUpAttributes_ShouldReturnLevelUpAttributes()

        {
            //arrange
            Rouge rouge = new Rouge();

            int[] expected = new int[] { 3, 10, 2, 11 };

            //act
            rouge.LevelUp(1);
            int[] actual = new int[] { rouge.Attributes.Strength, rouge.Attributes.Dexterity, rouge.Attributes.Intelligence, rouge.Attributes.Vitality };

            //assert

            Assert.Equal(expected, actual);


        }

        [Fact]
        public void NewRanger_CheckDeafultAttributes_ShouldReturnDeafultAttributes()

        {
            //arrange
            Ranger ranger = new Ranger();

            int[] expected = new int[] { 1, 7, 1, 8 };

            //act

            int[] actual = new int[] { ranger.Attributes.Strength, ranger.Attributes.Dexterity, ranger.Attributes.Intelligence, ranger.Attributes.Vitality };

            //assert

            Assert.Equal(expected, actual);


        }

        [Fact]
        public void LevelUpRanger_CheckLevelUpAttributes_ShouldReturnLevelUpAttributes()

        {
            //arrange
            Ranger ranger = new Ranger();

            int[] expected = new int[] { 2, 12, 2, 10 };

            //act
            ranger.LevelUp(1);
            int[] actual = new int[] { ranger.Attributes.Strength, ranger.Attributes.Dexterity, ranger.Attributes.Intelligence, ranger.Attributes.Vitality };

            //assert

            Assert.Equal(expected, actual);


        }


        [Fact]
        public void NewWarrior_CheckDeafultAttributes_ShouldReturnDeafultAttributes()

        {
            //arrange
            Warrior warrior = new Warrior();

            int[] expected = new int[] { 5, 2, 1, 10 };

            //act

            int[] actual = new int[] { warrior.Attributes.Strength, warrior.Attributes.Dexterity, warrior.Attributes.Intelligence, warrior.Attributes.Vitality };

            //assert

            Assert.Equal(expected, actual);


        }

        [Fact]
        public void LevelUpWarrior_CheckLevelUpAttributes_ShouldReturnLevelUpAttributes()

        {
            //arrange
            Warrior warrior = new Warrior();
            
            int[] expected = new int[] { 8,4, 2, 15 };

            //act
            warrior.LevelUp(1);
            int[] actual = new int[] { warrior.Attributes.Strength, warrior.Attributes.Dexterity, warrior.Attributes.Intelligence, warrior.Attributes.Vitality };

            //assert

            Assert.Equal(expected, actual);


        }

        [Fact]
        public void CalculateSecondaryAttributes_CheckSecondaryAttributesOnLevelledUpWarrior_ShouldReturnSecondaryAttributesOnLevelledUpWarrior()

        {
            //arrange
            Warrior warrior = new Warrior();

            int[] expected = new int[] { 150, 12, 2 };

            //act
            warrior.LevelUp(1);
            warrior.CalculateSecondaryAttributes();
            int[] actual = new int[] { warrior.SecondaryAttributes.Health, warrior.SecondaryAttributes.ArmourRating, warrior.SecondaryAttributes.ElementalResistance };

            //assert

            Assert.Equal(expected, actual);

        }

    }

}

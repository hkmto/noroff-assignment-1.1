﻿using MyAssignemnt.Items;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyAssignemnt
{
    public class Warrior : Hero
    {
        public Warrior()
        {
            Attributes = new PrimaryAttributes { Strength = 5, Dexterity = 2, Intelligence = 1, Vitality = 10};
        }

        public override void EquipWeapon(Weapon weapon)
        {

            if (weapon.ItemLevel > Level)

                throw new InvalidLevelForThisItem("Your hero's level is to low to equip this weapon");

            if (!(weapon.WeaponType == WeaponType.Axe || weapon.WeaponType == WeaponType.Sword || weapon.WeaponType == WeaponType.Hammer))

                throw new InvalidWeaponType("This weapon is not allowed for this character");

            if (weapon.ItemSlot == Slot.Weapon)

                Inventory.Add(weapon.ItemSlot, weapon);

            else

                throw new InvalidSlotForWeapon("Your hero cannot equip this weapon in this slot");
        }

        public override void EquipArmour(Armour armour)

        {
            if (armour.ItemLevel > Level)

                throw new InvalidLevelForThisItem("Your hero's level is to low to equip this armour");

            if (!(armour.ArmourType == ArmourType.Mail || armour.ArmourType == ArmourType.Plate))

                throw new InvalidArmourType("This armour is not allowed for this character");


            if (armour.ItemSlot != Slot.Weapon)

                Inventory.Add(armour.ItemSlot, armour);

            else

                throw new InvalidSlotForArmour("Your hero cannot equip this armour in this slot");

        }

        public override void LevelUp(int level)

        {
            if (level < 1)
                throw new InvalidLevel("Level must be greater than 1");

            Level += level;
            Attributes.Strength = Attributes.Strength + 3;
            Attributes.Dexterity = Attributes.Dexterity + 2;
            Attributes.Intelligence = Attributes.Intelligence + 1;
            Attributes.Vitality = Attributes.Vitality + 5;

        }


        public override double CalculateHeroDamagePerSecond()
        {

            return CalculateEquippedWeaponDPS() * (1 + (double)TotalAttributes().Strength / 100);
        }


    }
}
